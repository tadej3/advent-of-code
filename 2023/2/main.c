#include <fcntl.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define PARSE_INT(VAR, TERM, NEXT, ON_NEXT)    \
  if (buf[i] == TERM) {                        \
    VAR = 0;                                   \
    for (int j = 0; j < strIdx; j++) {         \
      VAR += str[j] * pow(10, strIdx - j - 1); \
    }                                          \
    strIdx = 0;                                \
    state = NEXT;                              \
    ON_NEXT                                    \
  } else {                                     \
    str[strIdx++] = buf[i] - '0';              \
  }

#define CHECK                                         \
  do {                                                \
    if (puzzlePart == 1) {                            \
      gameValid = gameValid && (bag[color] >= count); \
    } else if (count > minCubes[color]) {             \
      minCubes[color] = count;                        \
    }                                                 \
  } while (0)

typedef enum {
  GAME_ID,
  COUNT,
  COLOR,
} State;

typedef enum { RED, GREEN, BLUE, MAX_COLOR } Color;

const int bag[MAX_COLOR] = {12, 13, 14};

int main(int argc, char **argv) {
  if (argc < 3) {
    printf("No file or puzzle part specified\n");
    printf("Usage: ./main <file> <puzzle-part>\n");
    return 1;
  }
  int puzzlePart = *(argv[2]) - '0';
  if (puzzlePart > 2 || puzzlePart < 1) {
    printf("Invalid puzzle part\n");
    return 1;
  }
  int f = open(argv[1], O_RDONLY);
  struct stat stat;
  if (fstat(f, &stat) != 0) {
    fprintf(stderr, "fstat failed\n");
    return 1;
  }
  char *buf = malloc(stat.st_size);
  if (read(f, buf, stat.st_size) == -1) {
    fprintf(stderr, "Read failed\n");
    return 1;
  }
  char *str = malloc(3);
  int strIdx = 0;
  int gameId = 0;
  int count = 0;
  int answer = 0;
  State state = GAME_ID;
  Color color = MAX_COLOR;
  bool gameValid = true;
  int minCubes[MAX_COLOR] = {0, 0, 0};
  for (int i = 5; i < stat.st_size; i++) {
    if (buf[i] == '\n') {
      CHECK;
      if (puzzlePart == 1) {
        if (gameValid) {
          answer += gameId;
        }
      } else {
        answer += minCubes[RED] * minCubes[GREEN] * minCubes[BLUE];
        memset(minCubes, 0, MAX_COLOR * sizeof(int));
      }
      state = GAME_ID;
      color = MAX_COLOR;
      gameValid = true;
      i += 5;
      continue;
    }
    switch (state) {
      case GAME_ID: {
        PARSE_INT(gameId, ':', COUNT, i++;);
        break;
      }
      case COUNT: {
        PARSE_INT(count, ' ', COLOR, {});
        color = MAX_COLOR;
        break;
      }
      case COLOR: {
        switch (buf[i]) {
          case 'r':
            i += 2;
            color = RED;
            break;
          case 'g':
            i += 4;
            color = GREEN;
            break;
          case 'b':
            i += 3;
            color = BLUE;
            break;
          case ',':
          case ';':
            state = COUNT;
            if (color >= MAX_COLOR) {
              fprintf(stderr, "[WARN] Unknown color: %d\n", color);
              break;
            }
            CHECK;
            i++;
            break;
          default:
            fprintf(stderr, "[WARN] Unknown color initial: %c\n", buf[i]);
            break;
        }
        break;
      }
    }
  }
  printf("%d\n", answer);
  free(str);
  free(buf);
  close(f);
  return 0;
}
