#include "lib.h"
#include <assert.h>
#include <stdio.h>

void testNoOverlap(void) {
  printf("No overlap: ");
  Range r = {
      .start = 79,
      .length = 14,
      .next = NULL,
      .lastMap = MAP_TYPES_MAX,
  };
  Map m = {
      .srcStart = 98,
      .destStart = 50,
      .length = 2,
      .next = NULL,
  };
  applyMap(&r, &m, SEED_TO_SOIL);
  assert(r.start == 79);
  assert(r.length == 14);
  assert(r.next == NULL);
  printf("PASS\n");
}

void testFullOverlap(void) {
  printf("Full overlap: ");
  Range r = {
      .start = 79,
      .length = 14,
      .next = NULL,
      .lastMap = MAP_TYPES_MAX,
  };
  Map m = {
      .srcStart = 50,
      .destStart = 52,
      .length = 48,
      .next = NULL,
  };
  applyMap(&r, &m, SEED_TO_SOIL);
  assert(r.start == 81);
  assert(r.length == 14);
  assert(r.next == NULL);
  printf("PASS\n");
}

void testLeftOverlap(void) {
  printf("Left overlap: ");
  Range r = {
      .start = 79,
      .length = 14,
      .next = NULL,
      .lastMap = MAP_TYPES_MAX,
  };
  Map m = {
      .srcStart = 83,
      .destStart = 52,
      .length = 20,
      .next = NULL,
  };
  applyMap(&r, &m, SEED_TO_SOIL);
  assert(r.start == 52);
  assert(r.length == 10);
  assert(r.next != NULL);
  Range *r2 = r.next;
  assert(r2->start == 79);
  assert(r2->length == 4);
  assert(r2->next == NULL);
  printf("PASS\n");
}

void testRightOverlap(void) {
  printf("Right overlap: ");
  Range r = {
      .start = 79,
      .length = 14,
      .next = NULL,
      .lastMap = MAP_TYPES_MAX,
  };
  Map m = {
      .srcStart = 60,
      .destStart = 52,
      .length = 20,
      .next = NULL,
  };
  applyMap(&r, &m, SEED_TO_SOIL);
  assert(r.start == 71);
  assert(r.length == 1);
  assert(r.next != NULL);
  Range *r2 = r.next;
  assert(r2->start == 80);
  assert(r2->length == 13);
  assert(r2->next == NULL);
  printf("PASS\n");
}

void testBothOverlap(void) {
  printf("Both overlap: ");
  Range r = {
      .start = 55,
      .length = 38,
      .next = NULL,
      .lastMap = MAP_TYPES_MAX,
  };
  Map m = {
      .srcStart = 60,
      .destStart = 52,
      .length = 20,
      .next = NULL,
  };
  applyMap(&r, &m, SEED_TO_SOIL);
  assert(r.start == 52);
  assert(r.length == 20);
  assert(r.next != NULL);
  Range *r2 = r.next;
  assert(r2->start == 80);
  assert(r2->length == 13);
  assert(r2->next != NULL);
  Range *r3 = r2->next;
  assert(r3->start == 55);
  assert(r3->length == 5);
  assert(r3->next == NULL);
  printf("PASS\n");
}

int main(void) {
  testNoOverlap();
  testFullOverlap();
  testLeftOverlap();
  testRightOverlap();
  testBothOverlap();
  return 0;
}
