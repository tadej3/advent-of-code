#include "lib.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IS_DIGIT(X) ((X) >= '0' && (X) <= '9')

// Found using regex search in input.txt
#define MAX_DIGITS 10

Error parseLong(char *buf, int *bufIdx, long *n) {
  char str[MAX_DIGITS] = {0};
  int strIdx = 0;
  while (IS_DIGIT(buf[*bufIdx])) {
    if (strIdx >= MAX_DIGITS) {
      fprintf(stderr, "str buffer overflow\n");
      return 1;
    }
    str[strIdx++] = buf[(*bufIdx)++];
  }
  *n = atol(str);
  return 0;
}

Error parseSeeds(char *buf, int *bufIdx, long *seeds, int *seedCount) {
  *bufIdx += 7; // Skip 'seeds: '
  long seed = 0;
  do {
    if (parseLong(buf, bufIdx, &seed)) {
      fprintf(stderr, "failed to parse long\n");
      return 1;
    }
    seeds[(*seedCount)++] = seed;
  } while (buf[(*bufIdx)++] == ' ');
  return 0;
}

void destroyList(ListItem *list) {
  if (list->next)
    destroyList(list->next);
  free(list);
}

Error parseMap(char *buf, int *bufIdx, int bufSize, Map **maps, MapType type) {
  Map *last = NULL;
  do {
    Map *m = malloc(sizeof(Map));
    memset(m, 0, sizeof(Map));
    if (!last)
      maps[type] = m;
    else
      last->next = m;
    if (parseLong(buf, bufIdx, &m->destStart))
      goto error;
    (*bufIdx)++;
    if (parseLong(buf, bufIdx, &m->srcStart))
      goto error;
    (*bufIdx)++;
    if (parseLong(buf, bufIdx, &m->length))
      goto error;
    (*bufIdx)++;
    last = m;
  } while (*bufIdx < bufSize && buf[*bufIdx] != '\n');
  return 0;
error:;
  destroyList((ListItem *)maps[type]);
  fprintf(stderr, "failed to parse map\n");
  return 1;
}

#define IS_SRC_OUT_OF_RANGE                                                    \
  (src < m->srcStart || src > (m->srcStart + m->length))

void mapValue(Map *map, long src, long *dest) {
  Map *m = map;
  while (m && IS_SRC_OUT_OF_RANGE)
    m = m->next;
  if (!m)
    *dest = src;
  else
    *dest = (src - m->srcStart) + m->destStart;
}

long nextInRange(long *seeds, int i) {
  long *start = seeds;
  long *length = seeds + 1;
  long previousLength = 0;
  while (*start != 0) {
    long n = i + *start - previousLength;
    if (n < *start + *length)
      return n;
    previousLength += *length;
    start = start + 2;
    length = length + 2;
  }
  return 0;
}

void printWithOffset(Range *range, long minNum) {
  for (long i = 0; i < range->start - minNum; i++)
    printf("   ");
  for (long i = range->start; i < range->start + range->length; i++) {
    if (i < 10)
      printf(" %ld ", i);
    else
      printf("%ld ", i);
  }
}

long minRangeStart(Range *ranges) {
  long minStart = ranges->start;
  for (Range *r = ranges; r != NULL; r = r->next)
    if (r->start < minStart)
      minStart = r->start;
  return minStart;
}

void printRanges(Range *ranges, long minStart) {
  for (Range *r = ranges; r != NULL; r = r->next) {
    printWithOffset(r, minStart);
    printf("\n");
  }
}

#define MAP_RANGE(R, MAP, TYPE)                                                \
  do {                                                                         \
    R->start = ((R->start - MAP->srcStart) + MAP->destStart);                  \
    R->lastMap = TYPE;                                                         \
  } while (0)

void applyMap(Range *ranges, Map *map, MapType type) {
  Range *r = ranges;
  long mapStart = map->srcStart;
  long mapEnd = map->srcStart + map->length - 1;
  for (; r != NULL; r = r->next) {
    if (r->lastMap != MAP_TYPES_MAX && r->lastMap >= type)
      continue;
    long rangeStart = r->start;
    long rangeLength = r->length;
    long rangeEnd = r->start + r->length - 1;
    if (rangeStart > mapEnd || (rangeStart < mapStart && rangeEnd < mapStart))
      continue;                                         // No overlap
    if (rangeStart >= mapStart && rangeEnd <= mapEnd) { // Full overlap
      MAP_RANGE(r, map, type);
    } else {                                              // Partial overlap
      if (rangeStart < mapStart && rangeEnd > mapStart) { // Overlap on the left
        Range *r2 = malloc(sizeof(Range));
        *r2 = (Range){
            .start = rangeStart,
            .length = mapStart - rangeStart,
            .next = r->next,
            .lastMap = MAP_TYPES_MAX,
        };
        *r = (Range){
            .start = mapStart,
            .length = rangeLength - r2->length,
            .next = r2,
            .lastMap = r->lastMap,
        };
      }
      if (rangeEnd > mapEnd) { // Overlap on the right
        // Update range start & length, in case we already modified the original
        // range above (in overlap left)
        rangeStart = r->start;
        rangeLength = r->length;
        Range *r2 = malloc(sizeof(Range));
        memset(r2, 0, sizeof(Range));
        *r2 = (Range){
            .start = mapEnd + 1,
            .length = rangeEnd - mapEnd,
            .next = r->next,
            .lastMap = MAP_TYPES_MAX,
        };
        *r = (Range){
            .start = rangeStart,
            .length = mapEnd + 1 - rangeStart,
            .next = r2,
            .lastMap = r->lastMap,
        };
      }
      MAP_RANGE(r, map, type);
    }
  }
}
