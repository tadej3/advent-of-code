#include <fcntl.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "lib.h"

#define DEBUG 0

// Just needs to be big enough to not overflow
#define MAX_SEEDS 50

const int mapHeaderLength[MAP_TYPES_MAX] = {17, 23, 24, 19, 25, 28, 25};

int main(int argc, char **argv) {
  if (argc < 3) {
    printf("No file or puzzle part specified\n");
    printf("Usage: ./main <file> <puzzle-part>\n");
    return 1;
  }
  int puzzlePart = *(argv[2]) - '0';
  if (puzzlePart > 2 || puzzlePart < 1) {
    fprintf(stderr, "Invalid puzzle part\n");
    return 1;
  }
  int f = open(argv[1], O_RDONLY);
  struct stat stat;
  if (fstat(f, &stat) != 0) {
    fprintf(stderr, "fstat failed\n");
    return 1;
  }
  if (!stat.st_size) {
    fprintf(stderr, "file is empty\n");
    return 1;
  }
  char *buf = malloc(stat.st_size);
  if (read(f, buf, stat.st_size) == -1) {
    fprintf(stderr, "Read failed\n");
    return 1;
  }
  int bufIdx = 0;
  long seeds[MAX_SEEDS] = {0};
  int seedCount = 0;
  Map *maps[MAP_TYPES_MAX] = {0};
  Range *ranges = NULL;
  if (parseSeeds(buf, &bufIdx, seeds, &seedCount)) {
    fprintf(stderr, "failed to parse seeds\n");
    return 1;
  }
  for (int i = 0; i < MAP_TYPES_MAX; i++) {
    bufIdx += 2 /* \n */ + mapHeaderLength[i];
    if (parseMap(buf, &bufIdx, stat.st_size, maps, i)) {
      fprintf(stderr, "failed to parse %d map\n", i);
      return 1;
    }
  }
  long minLoc = LONG_MAX;
  if (puzzlePart == 1) {
    for (long i = 0; i < seedCount; i++) {
      long mapped = seeds[i];
      for (MapType type = SEED_TO_SOIL; type < MAP_TYPES_MAX; type++) {
        mapValue(maps[type], mapped, &mapped);
      }
      if (mapped < minLoc)
        minLoc = mapped;
    }
  } else {
    for (int i = 0; i < seedCount; i += 2) {
      Range *r = malloc(sizeof(Range));
      memset(r, 0, sizeof(Range));
      *r = (Range){
        .start = seeds[i],
        .length = seeds[i + 1],
        .next = ranges,
        .lastMap = MAP_TYPES_MAX,
      };
      ranges = r;
    }
    for (MapType type = SEED_TO_SOIL; type < MAP_TYPES_MAX; type++) {
#if DEBUG
      printf("---------------------------------\n");
      printf("Map Type: %d\n", type);
#endif
      Map *m = maps[type];
      for (; m != NULL; m = m->next) {
#if DEBUG
        printf("---------------------------------\n");
        printf("Src Start: %ld\n", m->srcStart);
        printf("Dest Start: %ld\n", m->destStart);
        printf("Length: %ld\n", m->length);
        printf("---------------------------------\n");
        long minOffset = minRangeStart(ranges);
        long tmp = minRangeStart((Range *)m);
        if (tmp < minOffset)
          minOffset = tmp;
        printRanges(ranges, minOffset);
        printf("---------------------------------\n");
        printRanges((Range *)m, minOffset);
        printf("---------------------------------\n");
#endif
        applyMap(ranges, m, type);
#if DEBUG
        printRanges(ranges, minOffset);
        printf("\n\n");
#endif
      }
    }
    minLoc = minRangeStart(ranges);
  }
  printf("%ld\n", minLoc);
  for (MapType type = SEED_TO_SOIL; type < MAP_TYPES_MAX; type++) {
    destroyList((ListItem *)maps[type]);
  }
  destroyList((ListItem *)ranges);
  free(buf);
  close(f);
  return 0;
}
