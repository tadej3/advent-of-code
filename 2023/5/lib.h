#pragma once

typedef enum {
  SEED_TO_SOIL,
  SOIL_TO_FERTALIZER,
  FERTALIZER_TO_WATER,
  WATER_TO_LIGHT,
  LIGHT_TO_TEMP,
  TEMP_TO_HUM,
  HUM_TO_LOC,
  MAP_TYPES_MAX
} MapType;

typedef struct _Range Range;
struct _Range {
  Range *next;
  long start;
  long length;
  MapType lastMap;
};

typedef struct _Map Map;
// Order of properties matters; must be castable into Range
struct _Map {
  Map *next;
  long srcStart;
  long length;
  long destStart;
};

typedef struct _ListItem ListItem;
struct _ListItem {
  ListItem *next;
};

typedef int Error;

Error parseSeeds(char *buf, int *bufIdx, long *seeds, int *seedCount);
Error parseMap(char *buf, int *bufIdx, int bufSize, Map **maps, MapType type);
void mapValue(Map *map, long src, long *dest);
long minRangeStart(Range *ranges);
void printRanges(Range *ranges, long minStart);
void applyMap(Range *ranges, Map *map, MapType type);
void destroyList(ListItem *list);
