#include <fcntl.h>
#include <limits.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define IS_DIGIT(X) ((X) >= '0' && (X) <= '9')
// Found using regex search in input.txt
#define MAX_DIGITS 10
// Just needs to be big enough to not overflow
#define MAX_SEEDS 50

#define NUM_THREADS 4
#define PARALLELISM_THRESHOLD_SEEDS 10000
#define ENABLE_PARALLELISM 1

typedef struct _Map Map;
struct _Map {
  long destStart;
  long srcStart;
  long length;
  Map *next;
};

typedef int Error;

typedef enum {
  SEED_TO_SOIL,
  SOIL_TO_FERTALIZER,
  FERTALIZER_TO_WATER,
  WATER_TO_LIGHT,
  LIGHT_TO_TEMP,
  TEMP_TO_HUM,
  HUM_TO_LOC,
  MAP_TYPES_MAX
} MapType;

typedef struct {
  long seedCount;
  int puzzlePart;
  long *seeds;
  Map **maps;
  long minLoc;
  long i;
  bool done;
} FindMinLocTask;

const int mapHeaderLength[MAP_TYPES_MAX] = {17, 23, 24, 19, 25, 28, 25};

Error parseLong(char *buf, int *bufIdx, long *n) {
  char str[MAX_DIGITS] = {0};
  int strIdx = 0;
  while (IS_DIGIT(buf[*bufIdx])) {
    if (strIdx >= MAX_DIGITS) {
      fprintf(stderr, "str buffer overflow\n");
      return 1;
    }
    str[strIdx++] = buf[(*bufIdx)++];
  }
  *n = atol(str);
  return 0;
}

Error parseSeeds(char *buf, int *bufIdx, long *seeds, long *seedCount) {
  *bufIdx += 7;  // Skip 'seeds: '
  long seed = 0;
  do {
    if (parseLong(buf, bufIdx, &seed)) {
      fprintf(stderr, "failed to parse long\n");
      return 1;
    }
    seeds[(*seedCount)++] = seed;
  } while (buf[(*bufIdx)++] == ' ');
  return 0;
}

void destroyList(Map *list) {
  if (list->next) destroyList(list->next);
  free(list);
}

Error parseMap(char *buf, int *bufIdx, Map **maps, MapType type) {
  Map *last = NULL;
  do {
    Map *m = malloc(sizeof(Map));
    memset(m, 0, sizeof(Map));
    if (!last)
      maps[type] = m;
    else
      last->next = m;
    if (parseLong(buf, bufIdx, &m->destStart)) goto error;
    (*bufIdx)++;
    if (parseLong(buf, bufIdx, &m->srcStart)) goto error;
    (*bufIdx)++;
    if (parseLong(buf, bufIdx, &m->length)) goto error;
    (*bufIdx)++;
    last = m;
  } while (buf[*bufIdx] != '\n');
  return 0;
error:;
  destroyList(maps[type]);
  fprintf(stderr, "failed to parse map\n");
  return 1;
}

#define IS_SRC_OUT_OF_RANGE (src < m->srcStart || src > (m->srcStart + m->length))

void mapValue(Map *map, long src, long *dest) {
  Map *m = map;
  while (m && IS_SRC_OUT_OF_RANGE) {
    m = m->next;
  }
  if (!m) {
    *dest = src;
  } else {
    *dest = (src - m->srcStart) + m->destStart;
  }
}

long nextInRange(long *seeds, int i) {
  long *start = seeds;
  long *length = seeds + 1;
  long previousLength = 0;
  while (*start != 0) {
    long n = i + *start - previousLength;
    if (n < *start + *length) return n;
    previousLength += *length;
    start = start + 2;
    length = length + 2;
  }
  return 0;
}

void *findClosestLocation(void *arg) {
  FindMinLocTask *a = (FindMinLocTask *)arg;
  for (; a->i < a->seedCount; a->i++) {
    long mapped = a->puzzlePart == 1 ? a->seeds[a->i] : nextInRange(a->seeds, a->i);
    for (MapType type = SEED_TO_SOIL; type < MAP_TYPES_MAX; type++) {
      mapValue(a->maps[type], mapped, &mapped);
    }
    if (mapped < a->minLoc) a->minLoc = mapped;
  }
  a->done = true;
  return NULL;
}

int main(int argc, char **argv) {
  if (argc < 3) {
    printf("No file or puzzle part specified\n");
    printf("Usage: ./main <file> <puzzle-part>\n");
    return 1;
  }
  int puzzlePart = *(argv[2]) - '0';
  if (puzzlePart > 2 || puzzlePart < 1) {
    fprintf(stderr, "Invalid puzzle part\n");
    return 1;
  }
  int f = open(argv[1], O_RDONLY);
  struct stat stat;
  if (fstat(f, &stat) != 0) {
    fprintf(stderr, "fstat failed\n");
    return 1;
  }
  if (!stat.st_size) {
    fprintf(stderr, "file is empty\n");
    return 1;
  }
  char *buf = malloc(stat.st_size);
  if (read(f, buf, stat.st_size) == -1) {
    fprintf(stderr, "Read failed\n");
    return 1;
  }
  int bufIdx = 0;
  long seeds[MAX_SEEDS] = {0};
  long seedCount = 0;
  Map *maps[MAP_TYPES_MAX] = {0};
  if (parseSeeds(buf, &bufIdx, seeds, &seedCount)) {
    fprintf(stderr, "failed to parse seeds\n");
    return 1;
  }
  for (int i = 0; i < MAP_TYPES_MAX; i++) {
    bufIdx += 2 /* \n */ + mapHeaderLength[i];
    if (parseMap(buf, &bufIdx, maps, i)) {
      fprintf(stderr, "failed to parse %d map\n", i);
      return 1;
    }
  }
  long minLoc = LONG_MAX;
  if (puzzlePart == 2) {
    long newSeedCount = 0;
    for (long i = 1; i < seedCount; i += 2) {
      newSeedCount += seeds[i];
    }
    seedCount = newSeedCount;
  }
  if (seedCount > PARALLELISM_THRESHOLD_SEEDS && ENABLE_PARALLELISM) {
    pthread_t threads[NUM_THREADS];
    FindMinLocTask *tasks[NUM_THREADS];
    long chunkSize = seedCount / NUM_THREADS;
    for (int i = 0; i < NUM_THREADS; i++) {
      FindMinLocTask *task = malloc(sizeof(FindMinLocTask));
      tasks[i] = task;
      memset(task, 0, sizeof(FindMinLocTask));
      task->i = i * chunkSize;
      task->minLoc = LONG_MAX;
      task->puzzlePart = puzzlePart;
      task->maps = maps;
      task->seeds = seeds;
      task->seedCount = (i + 1) * chunkSize + (i + 1 == NUM_THREADS ? (seedCount % NUM_THREADS) : 0);
      int ret = pthread_create(&threads[i], NULL, findClosestLocation, (void *)task);
      if (ret) {
        fprintf(stderr, "failed to start thread %d: %d\n", i, ret);
        return 1;
      }
    }
    long seedsChecked = 0;
    bool allDone = true;
    do {
      sleep(1);
      seedsChecked = 0;
      allDone = true;
      for (int i = 0; i < NUM_THREADS; i++) {
        seedsChecked += tasks[i]->i - chunkSize * i;
        allDone = allDone && tasks[i]->done;
        if (!tasks[i]->done) continue;
        if (tasks[i]->minLoc < minLoc) minLoc = tasks[i]->minLoc;
      }
      printf("\rProgress %lf%%", seedsChecked / (double)seedCount * 100);
      fflush(stdout);
    } while (!allDone);
    printf("\33[2K\r");
    fflush(stdout);
  } else {
    FindMinLocTask task = {
        .seedCount = seedCount, .seeds = seeds, .i = 0, .maps = maps, .puzzlePart = puzzlePart, .minLoc = LONG_MAX};
    findClosestLocation((void *)&task);
    minLoc = task.minLoc;
  }
  printf("%ld\n", minLoc);
  for (MapType type = SEED_TO_SOIL; type < MAP_TYPES_MAX; type++) {
    destroyList(maps[type]);
  }
  free(buf);
  close(f);
  return 0;
}
