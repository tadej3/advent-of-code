#!/bin/sh
PART=$1
RESULT=$(./main example$PART.txt $PART)
echo "$RESULT"
if [ $PART -eq 1 ]; then
  [ "$RESULT" == "4361" ] && echo "PASS" || echo "FAIL"
elif [ $PART -eq 2 ]; then
  [ "$RESULT" == "467835" ] && echo "PASS" || echo "FAIL"
fi
