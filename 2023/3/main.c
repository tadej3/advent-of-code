#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define IS_DIGIT(X) ((X) >= '0' && (X) <= '9')
#define IS_SYMBOL(X)                                                                          \
  ((((X) >= '!' && (X) <= '/') || ((X) >= ':' && (X) <= '@') || ((X) >= '[' && (X) <= '`') || \
    ((X) >= '{' && (X) <= '~')) &&                                                            \
   (X) != '.')
// Size of the gear candidate buffer; chosen arbitrarily, just needs to be large enough to avoid an overflow
#define MAX_GEARS 20

typedef struct {
  int val;
  int i;
} GearCandidate;

bool hasAdjacentSymbol(char *buf, int i, int s, int l) {
  if (IS_SYMBOL(buf[i - 1])) return true;                       // Symbol to the left?
  if (i + 1 < s && IS_SYMBOL(buf[i + 1])) return true;          // Symbol to the right?
  if (i - l > 0 && IS_SYMBOL(buf[i - l])) return true;          // Symbol above?
  if (i + l < s && IS_SYMBOL(buf[i + l])) return true;          // Symbol below?
  if (i - l - 1 > 0 && IS_SYMBOL(buf[i - l - 1])) return true;  // Symbol top left?
  if (i - l + 1 > 0 && IS_SYMBOL(buf[i - l + 1])) return true;  // Symbol top right?
  if (i + l - 1 > 0 && IS_SYMBOL(buf[i + l - 1])) return true;  // Symbol bottom left?
  if (i + l + 1 > 0 && IS_SYMBOL(buf[i + l + 1])) return true;  // Symbol bottom right?
  return false;
}

bool isGearCandidate(char *buf, int i, int s, int l, int *symbolIdx) {
  if (i - 1 > 0 && buf[i - 1] == '*') {  // Symbol to the left?
    *symbolIdx = i - 1;
    return true;
  }
  if (i + 1 < s && buf[i + 1] == '*') {  // Symbol to the right?
    *symbolIdx = i + 1;
    return true;
  }
  if (i - l > 0 && buf[i - l] == '*') {  // Symbol above?
    *symbolIdx = i - l;
    return true;
  }
  if (i + l < s && buf[i + l] == '*') {  // Symbol below?
    *symbolIdx = i + l;
    return true;
  }
  if (i - l - 1 > 0 && buf[i - l - 1] == '*') {  // Symbol top left?
    *symbolIdx = i - l - 1;
    return true;
  }
  if (i - l + 1 > 0 && buf[i - l + 1] == '*') {  // Symbol top right?
    *symbolIdx = i - l + 1;
    return true;
  }
  if (i + l - 1 < s && buf[i + l - 1] == '*') {  // Symbol bottom left?
    *symbolIdx = i + l - 1;
    return true;
  }
  if (i + l + 1 < s && buf[i + l + 1] == '*') {  // Symbol bottom right?
    *symbolIdx = i + l + 1;
    return true;
  }
  return false;
}

bool insertGearCandidate(GearCandidate **gears, GearCandidate *gear) {
  for (int j = 0; j < MAX_GEARS; j++) {
    if (!gears[j]) {
      gears[j] = gear;
      return true;
    }
  }
  return false;
}

bool isGearCandidateOutOfReach(GearCandidate **gears, int i, int pos, int l) {
  if (gears[i]->i >= pos + 2 * l) {  // Are we already more than 1 line above the symbol? This isn't a gear
    free(gears[i]);
    gears[i] = NULL;
    return true;
  }
  if (gears[i]->i <= pos - 2 * l) {  // Are we already more than 1 line below the symbol? This isn't a gear
    free(gears[i]);
    gears[i] = NULL;
    return true;
  }
  return false;
}

int calcGearRatio(GearCandidate **gears, int pos, int l) {
  int result = 0;
  for (int i = 0; i < MAX_GEARS; i++) {
    GearCandidate *gear = gears[i];
    if (!gear) continue;
    if (isGearCandidateOutOfReach(gears, i, pos, l)) continue;
    for (int j = 0; j < MAX_GEARS; j++) {
      if (!gears[j] || i == j) continue;
      if (isGearCandidateOutOfReach(gears, j, pos, l)) continue;
      if (gear->i == gears[j]->i) {
        result += gear->val * gears[j]->val;
        free(gears[i]);
        free(gears[j]);
        gears[i] = NULL;
        gears[j] = NULL;
        goto outer;
      }
    }
outer:;
  }
  return result;
}

int main(int argc, char **argv) {
  if (argc < 3) {
    printf("No file or puzzle part specified\n");
    printf("Usage: ./main <file> <puzzle-part>\n");
    return 1;
  }
  int puzzlePart = *(argv[2]) - '0';
  if (puzzlePart > 2 || puzzlePart < 1) {
    printf("Invalid puzzle part\n");
    return 1;
  }
  int f = open(argv[1], O_RDONLY);
  struct stat stat;
  if (fstat(f, &stat) != 0) {
    fprintf(stderr, "fstat failed\n");
    return 1;
  }
  char *buf = malloc(stat.st_size);
  if (read(f, buf, stat.st_size) == -1) {
    fprintf(stderr, "Read failed\n");
    return 1;
  }
  char *str = malloc(4);
  memset(str, 0, 4);
  int strIdx = 0;
  int answer = 0;
  bool isValid = false;
  int lineLen = 0;
  GearCandidate *gears[MAX_GEARS] = {0};
  int gearSymbolIdx = 0;
  for (int i = 0; i < stat.st_size; i++) {
    lineLen++;
    if (buf[i] == '\n') break;
  }
  for (int i = 0; i < stat.st_size; i++) {
    if (IS_DIGIT(buf[i])) {
      if (strIdx == 3) {
        fprintf(stderr, "str buffer overflow\n");
        goto end;
      }
      str[strIdx++] = buf[i];
      if (puzzlePart == 1) {
        isValid |= hasAdjacentSymbol(buf, i, (int)stat.st_size, lineLen);
      } else {
        isValid |= isGearCandidate(buf, i, (int)stat.st_size, lineLen, &gearSymbolIdx);
      }
      if (i + 1 >= stat.st_size || !IS_DIGIT(buf[i + 1])) {  // We're at the end of a number
        if (isValid) {
          if (puzzlePart == 1) {
            answer += atoi(str);
          } else {
            GearCandidate *gear = malloc(sizeof(GearCandidate));
            gear->val = atoi(str);
            gear->i = gearSymbolIdx;
            if (!insertGearCandidate(gears, gear)) {
              fprintf(stderr, "Gear candidate buffer overflow\n");
              goto end;
            }
            answer += calcGearRatio(gears, i, lineLen);
          }
        }
        gearSymbolIdx = 0;
        isValid = false;
        strIdx = 0;
        memset(str, 0, 4);
      }
    }
  }
  printf("%d\n", answer);
end:;
  for (int i = 0; i < MAX_GEARS; i++) {
    if (gears[i]) free(gears[i]);
  }
  free(str);
  free(buf);
  close(f);
  return 0;
}
