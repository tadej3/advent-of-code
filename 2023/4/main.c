#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

// All chosen based on the input file; just need to be large enough to avoid overflowing
#define MAX_WINNING_NUMS 10
#define MAX_GIVEN_NUMS 30
#define MAX_CARDS 256

#define IS_DIGIT(X) ((X) >= '0' && (X) <= '9')

typedef enum {
  PREAMBLE,
  WINNING,
  GIVEN,
} State;

int scoreCard(int *given, int *winning, int numCount, int winningCount) {
  int score = 0;
  for (int i = 0; i < numCount; i++) {
    for (int j = 0; j < winningCount; j++) {
      if (given[i] == winning[j]) {
        if (score == 0)
          score = 1;
        else
          score *= 2;
        break;
      }
    }
  }
  return score;
}

int calcCardWins(int *given, int *winning, int numCount, int winningCount) {
  int wins = 0;
  for (int i = 0; i < numCount; i++) {
    for (int j = 0; j < winningCount; j++) {
      if (given[i] == winning[j]) {
        wins++;
        break;
      }
    }
  }
  return wins;
}

int exhaustCardPile(int idx, int *cardWins) {
  int size = 1;
  for (int i = 0; i < cardWins[idx]; i++) {
    size += exhaustCardPile(idx + i + 1, cardWins);
  }
  return size;
}

int main(int argc, char **argv) {
  if (argc < 3) {
    printf("No file or puzzle part specified\n");
    printf("Usage: ./main <file> <puzzle-part>\n");
    return 1;
  }
  int puzzlePart = *(argv[2]) - '0';
  if (puzzlePart > 2 || puzzlePart < 1) {
    printf("Invalid puzzle part\n");
    return 1;
  }
  int f = open(argv[1], O_RDONLY);
  struct stat stat;
  if (fstat(f, &stat) != 0) {
    fprintf(stderr, "fstat failed\n");
    return 1;
  }
  char *buf = malloc(stat.st_size);
  if (read(f, buf, stat.st_size) == -1) {
    fprintf(stderr, "Read failed\n");
    return 1;
  }
  int answer = 0;
  State state = PREAMBLE;
  int winning[MAX_WINNING_NUMS] = {0};
  int winningIdx = 0;
  int winningCount = 0;
  int given[MAX_GIVEN_NUMS] = {0};
  int givenIdx = 0;
  int givenCount = 0;
  char str[3] = {0};
  int strIdx = 0;
  int cardWins[MAX_CARDS] = {0};
  int cardIdx = 0;
  bool parsedFirstCard = false;
  for (int i = 0; i < stat.st_size; i++) {
    switch (state) {
      case PREAMBLE: {
        if (buf[i] == ':') {
          i++;
          state = WINNING;
        }
        break;
      }

      case WINNING: {
        if (IS_DIGIT(buf[i])) {
          if (strIdx >= 3) {
            fprintf(stderr, "str buffer overflow\n");
            goto end;
          }
          str[strIdx++] = buf[i];
        } else if (buf[i] == ' ') {
          if (winningIdx >= MAX_WINNING_NUMS) {
            fprintf(stderr, "winning nums buffer overflow\n");
            goto end;
          }
          int n = atoi(str);
          if (n) {
            winning[winningIdx++] = atoi(str);
            if (!parsedFirstCard) winningCount++;
          }
          memset(str, 0, 3);
          strIdx = 0;
        } else if (buf[i] == '|') {
          if (strIdx != 0) {
            fprintf(stderr, "invalid format. didn't finish parsing last winning number\n");
            goto end;
          }
          i++;
          winningIdx = 0;
          state = GIVEN;
        }
        break;
      }

      case GIVEN: {
        if (IS_DIGIT(buf[i])) {
          if (strIdx >= 3) {
            fprintf(stderr, "str buffer overflow\n");
            goto end;
          }
          str[strIdx++] = buf[i];
        } else if (buf[i] == ' ' || buf[i] == '\n') {
          if (givenIdx >= MAX_GIVEN_NUMS) {
            fprintf(stderr, "given nums buffer overflow\n");
            goto end;
          }
          int n = atoi(str);
          if (n) {
            given[givenIdx++] = atoi(str);
            if (!parsedFirstCard) givenCount++;
          }
          memset(str, 0, 3);
          strIdx = 0;
          if (buf[i] == '\n') {
            if (puzzlePart == 1) {
              answer += scoreCard(given, winning, givenCount, winningCount);
            } else {
              cardWins[cardIdx++] = calcCardWins(given, winning, givenCount, winningCount);
            }
            givenIdx = 0;
            parsedFirstCard = true;
            state = PREAMBLE;
          }
        }
        break;
      }
    }
  }
  if (puzzlePart == 2) {
    for (int i = 0; i < cardIdx; i++) {
      answer += exhaustCardPile(i, cardWins);
    }
  }
  printf("%d\n", answer);
end:;
  free(buf);
  close(f);
  return 0;
}
