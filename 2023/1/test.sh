#!/bin/sh
PART=$1
RESULT=$(./main example$PART.txt $PART)
echo "$RESULT"
if [ $PART -eq 1 ]; then
  [ "$RESULT" == "142" ] && echo "PASS" || echo "FAIL"
elif [ $PART -eq 2 ]; then
  [ "$RESULT" == "281" ] && echo "PASS" || echo "FAIL"
fi
