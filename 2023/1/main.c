#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define COMMIT_OR_MATCH(IDX, STR1, DIGIT1, STR2, DIGIT2, STR3, DIGIT3) \
  if (*strIdx <= IDX) {                                                \
    if (!memcmp(str, STR1, *strIdx)) {                                 \
      if (*strIdx == IDX) {                                            \
        *current = DIGIT1;                                             \
      } else {                                                         \
        match = true;                                                  \
      }                                                                \
    } else if (!memcmp(str, STR2, *strIdx)) {                          \
      if (*strIdx == IDX) {                                            \
        *current = DIGIT2;                                             \
      } else {                                                         \
        match = true;                                                  \
      }                                                                \
    } else if (!memcmp(str, STR3, *strIdx)) {                          \
      if (*strIdx == IDX) {                                            \
        *current = DIGIT3;                                             \
      } else {                                                         \
        match = true;                                                  \
      }                                                                \
    }                                                                  \
  }

void strToDigit(char *str, int *strIdx, char next, int *current) {
  str[(*strIdx)++] = next;
  bool match = false;
  while (*strIdx > 0 && !match) {
    COMMIT_OR_MATCH(3, "one", 1, "two", 2, "six", 6);
    COMMIT_OR_MATCH(4, "four", 4, "five", 5, "nine", 9);
    COMMIT_OR_MATCH(5, "three", 3, "seven", 7, "eight", 8);
    if (!match) {
      for (int i = 1; i < *strIdx; i++) {
        str[i - 1] = str[i];
      }
      (*strIdx)--;
    }
  }
  if (*strIdx == 5 || !match) {
    *strIdx = 0;
  }
}

int main(int argc, char **argv) {
  if (argc < 3) {
    printf("No file or puzzle part specified\n");
    printf("Usage: ./main <file> <puzzle-part>\n");
    return 1;
  }
  int puzzlePart = *(argv[2]) - '0';
  if (puzzlePart > 2 || puzzlePart < 1) {
    printf("Invalid puzzle part\n");
    return 1;
  }
  int f = open(argv[1], O_RDONLY);
  struct stat stat;
  if (fstat(f, &stat) != 0) {
    fprintf(stderr, "fstat failed\n");
    return 1;
  }
  char *buf = malloc(stat.st_size);
  int totalSum = 0;
  int lineSum = 0;
  int current = 0;
  bool foundFirst = false;
  // Longest digits are 5 letters long (three, seven, eight)
  char *str = malloc(5);
  int strIdx = 0;
  if (read(f, buf, stat.st_size) == -1) {
    fprintf(stderr, "Read failed\n");
    return 1;
  }
  for (int i = 0; i < stat.st_size; i++) {
    if (buf[i] >= '0' && buf[i] <= '9') {
      current = buf[i] - '0';
    } else {
      strToDigit(str, &strIdx, buf[i], &current);
    }
    if (!foundFirst && current) {
      lineSum += current * 10;
      foundFirst = true;
      strIdx = 0;
    }
    if (buf[i] == '\n') {
      lineSum += current;
      totalSum += lineSum;
      lineSum = 0;
      current = 0;
      strIdx = 0;
      foundFirst = false;
      continue;
    }
  }
  lineSum += current;
  totalSum += lineSum;
  printf("%d\n", totalSum);
  free(str);
  free(buf);
  close(f);
  return 0;
}
